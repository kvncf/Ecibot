var builder = require('botbuilder');
var menusOT = require('./data/menuOT');
var menusFB = require('./data/menuFB');

module.exports = [
    
    // Inicio del Dialogo de Saludo
	function(session, args, next) {
		session.sendTyping();
		if (!session.userData.nombre) {
			setTimeout(function () {
                var cardGif = new builder.HeroCard(session)
                .images([
                    builder.CardImage.create(session, "https://media.giphy.com/media/3oFzlYDOPUI0Je07M4/giphy.gif")
                ]);

                /* var gifImage = new builder.AnimationCard(session)
                    .title()
                    .subtitle('')
                    .text('')
                    .media([{ profile: 'GIF test',
                        url: 'https://media.giphy.com/media/3oFzlYDOPUI0Je07M4/giphy.gif'
                    }]); */
            
                // Adjuntamos la tarjeta al mensaje
                var msj = new builder.Message(session).addAttachment(cardGif);
                session.send(msj);
				builder.Prompts.text(session, 'Hola soy ECIBOT🤖😄!, soy el nuevo asistente virtual de la Escuela Colombiana de Ingeniería. \n\nEstoy en versión de PRUEBA, mi lenguaje es aún corto, si alguna pregunta no puedo responder hazmelo saber con una SUGERENCIA. \n\nEmpecemos. ¿Cuál es tú nombre?');
			}, 2000);
        }
        else {
            next();
        }
	},

    // Advertencias
	function (session, args, next) {
        if (args.response) {
            var msj = args.response.toUpperCase();
            session.userData.nombre = msj;
        }

        session.send('RECUERDA: Estoy en versión de PRUEBA, mi lenguaje es aún corto.');
        if (session.message.source === "facebook"){
            session.send(`Bueno una vez aclarado lo anterior😄, ${session.userData.nombre}, te puedo ayudar con: `);
            session.send('Empieza a deslizar de derecha a izquierda');
        }else{
            session.send(`Bueno una vez aclarado lo anterior😄, ${session.userData.nombre}, te puedo ayudar con: `);
        }
		
		next();
	}, 
    
    // Lista de servicios (Segmentar por canales)
	function(session, args, next) {
        var cards = [];
        // Menu´para Facebook
        if (session.message.source === "facebook"){
            menusFB.forEach(function(menu) {
                var card = new builder.HeroCard(session)
                    .title(menu.name)
                    .subtitle(menu.subtitle)
                    .text(menu.text)
                    .images([
                        builder.CardImage.create(session, menu.image)
                    ])
                    .buttons([
                        builder.CardAction.postBack(session, menu.url, 'Empezar')
                    ])
                    cards.push(card);
                });
        }else{
            // Otros canales
            var menuECI = menusOT[0];
            var menuComunidad = menusOT[1];
    
            // Card de Servicios ECI
            var cardECI = new builder.HeroCard(session)
                    .title(menuECI.name)
                    .subtitle(menuECI.subtitle)
                    .text(menuECI.text)
                    .images([
                        builder.CardImage.create(session, menuECI.image)
                    ])
                    .buttons([
                        builder.CardAction.postBack(session, menuECI.url1, menuECI.opcion1),
                        builder.CardAction.postBack(session, menuECI.url2, menuECI.opcion2)
                    ]);
    
            // Card de Funcionalidades de la comunidad
            var cardComunidad = new builder.HeroCard(session)
                    .title(menuComunidad.name)
                    .subtitle(menuComunidad.subtitle)
                    .text(menuComunidad.text)
                    .images([
                        builder.CardImage.create(session, menuComunidad.image)
                    ])
                    .buttons([
                        builder.CardAction.postBack(session, menuComunidad.url1, menuComunidad.opcion1),
                        builder.CardAction.postBack(session, menuComunidad.url2, menuComunidad.opcion2),
                        builder.CardAction.postBack(session, menuComunidad.url3, menuComunidad.opcion3),
                        builder.CardAction.postBack(session, menuComunidad.url4, menuComunidad.opcion4),
                        builder.CardAction.postBack(session, menuComunidad.url5, menuComunidad.opcion5)
                    ]);
                    
            cards.push(cardECI);
            cards.push(cardComunidad);
        }

        var reply = new builder.Message(session)
            .attachmentLayout(builder.AttachmentLayout.carousel)
            .attachments(cards);

        session.endConversation(reply);
    }
]

