var builder = require('botbuilder');
var request = require('ajax-request');
var dotenv = require('dotenv');

// Para utilizar variables de entorno
dotenv.config();

module.exports = [
	function(session, args, next) {
		// KSSPSCORE:
		// Hacer peticion de uso con metrica NOTI
		if (!args) args = { intent: { entities: {} } };
		// Buscar entidades
        var categoria = builder.EntityRecognizer.findEntity(args.entities, 'noticia.categoria');
        var keyword = builder.EntityRecognizer.findEntity(args.entities, 'noticia.keyword');
		//borramos la copia 
		session.dialogData.categoria = null;
		session.dialogData.keyword = null;
		session.sendTyping();
		setTimeout(function () {
			if (categoria) {
				session.send('📚 Fuente NewsAPI.org');
				if (keyword){
					session.dialogData.categoria = categoria.entity.toUpperCase();
					next({ response: categoria, keyword: keyword.entity });
				}else{
					session.dialogData.categoria = categoria.entity.toUpperCase();
					next({response: categoria});
				}
			} else {
				builder.Prompts.choice(session, '¿De cuál sección quieres las noticias? 🔍', 'GENERAL|CIENCIA|TECNOLOGIA|SALUD|DEPORTES', { listStyle: builder.ListStyle.button });
			}
		}, 2000);
		
	},

	/**
	 * Validar si viene con grupo o sino pedirlo
	 */
	function(session, results, next) {
		if (results.keyword){
			session.dialogData.keyword = results.keyword;
			next();
		}else{
            var categoria = results.response.entity.toUpperCase();
            session.dialogData.categoriaES = categoria;
            var catQuery = "";
            switch(categoria) {
                case "GENERAL":
                    catQuery = "general";
                    break;
                case "CIENCIA":
                    catQuery = "science";
                    break;
                case "TECNOLOGIA":
                    catQuery = "technology";
                    break;
                case "SALUD":
                    catQuery = "health";
                    break;
                case "DEPORTES":
                    catQuery = "sports";
                    break;
                case "ENTRETENIMIENTO":
                    catQuery = "entertainment";
                    break;
                default:
                    catQuery = null;
            }

            session.dialogData.categoria = catQuery;
            next();
		}
	},

	/**
	 * Enviar mensaje previo y validar numero de grupo
	 */
	function (session, results, next){
        var url = "";
        session.sendTyping();
        
        if(session.dialogData.categoria){
            session.send('Ya mismo estoy buscando, esto tomará un tiempo 🔍');
            var cardGif = new builder.HeroCard(session)
                .images([
                    builder.CardImage.create(session, "https://media.giphy.com/media/3oEduPlMkw4LZE7624/giphy.gif")
                ]);
        
            // Adjuntamos la tarjeta al mensaje
            var msj = new builder.Message(session).addAttachment(cardGif);
            session.send(msj);
        
            // Validar la sigla
            if (session.dialogData.keyword){
                url = process.env.SERVER_NEWS + '&category=' + session.dialogData.categoria + '&q=' + session.dialogData.keyword;
            }else{
                url = process.env.SERVER_NEWS + '&category=' + session.dialogData.categoria;
            }
            //console.log('# ----------------- > ' + url);
            next({ response: url });
        }else{
            session.send("Verifica la sección de la noticia, por favor!👋");
            session.beginDialog("Noticias");
        }
	},

	/**
	 * Hacer request a EciUtils
	 */
	function (session, results, next) {
		request(results.response, function (err, res, body) {
			//si no hay conexión con el servidor
			if (body == undefined) {
				session.endConversation("Parece que Kevin no ha configurado bien la conexión con el servidor de noticias, porque no me puedo conectar.");
				return false;
			}
			if (body == "null") {
				session.send("Upsss😭🙄. No encontre nada. ");
				session.send("Intentémoslo de nuevo 😇");
				return false;
			}

			//verificamos que la respuesta del servidor sea JSON
			try {
				var J = eval("(" + body + ")");
			} catch (e) {
				var J = Array("res");
				J["error"] = true;
			}

			if (J["error"])
				session.endConversation("Ha ocurrido un error con la conexión: " + body);
			else if (J.length == 0){
				session.send("Upsss😭🙄.. No encontre nada, verifica el grupo y/o la materia y vuelve a intentarlo.");
				next();
			}
			else {
                //guardamos una copia para poder seguir usando
                var listaJson = J;

                var cards = [];
                if (listaJson.status === "ok"){
                    listaJson.articles.forEach(function (article){
                        var card = new builder.ThumbnailCard(session)
                            .title(article.title)
                            .subtitle(article.source.name)
                            .text(article.description)
                            .images([
                                builder.CardImage.create(session, article.urlToImage)
                            ])
                            .buttons([
                                builder.CardAction.openUrl(session, article.url, '📖 Leer Artículo')
                            ])
                        
                            cards.push(card);
                    });

                    var reply = new builder.Message(session)
                        .attachmentLayout(builder.AttachmentLayout.carousel)
                        .attachments(cards);
                    
                    session.send(`Aquí están las noticias que solicitaste de ${session.dialogData.categoriaES}.`)

                    session.endConversation(reply);
                }else{
                    session.endConversation("Parece que hay un problema con el servidor de noticias, no me puedo conectar.");
				    return false;
                }
			}
		});
	},

]