var builder = require('botbuilder');
var dotenv = require('dotenv');
var ecibot = require('../extra/ecibot');
var ecibotAjaxJson = require('../extra/ecibotAjaxJson');

module.exports = [
	/**
	 * recibe el aviso de ayuda pero intenta capturar el mensaje
	 */
	function (session, args, next) {
		// KSSPSCORE:
		// Hacer peticion de uso con metrica EMERG
		// Buscar entidades de seguridad
		var seguridad = builder.EntityRecognizer.findEntity(args.entities, 'Emergencia.seguridad');
		session.sendTyping();
		session.userData.situacionEmergencia=session.message.text;
		session.userData.situacionEmergenciaSeguridad=seguridad?true:false;//detectamos si es de seguridad o no
		builder.Prompts.confirm(session, '¿Estás seguro de notificar la emergencia? 🤔, esto es muy serio!!');
	},

	/**
	 * Solicitar informacion
	 */
	function (session, args, next) {
		session.sendTyping();
		if (args.response) {
			builder.Prompts.text(session, 'Un momento para ayudarte necesito más información como: 📋Descripción la situación, lugar y 📲número de teléfono.');
		} else {
			session.endConversation("Buena decisión!👍. Recuerda escribir 'MENU' para volver al inicio 👋");
		}
	},

	/**
	 * si es de seguridad guardamos sino verificamos si el nuevo mensaje contiene temas de seguridad
	 */
	function (session, args, next) {
		session.userData.situacionEmergencia+=", "+args.response;

		if(session.userData.situacionEmergenciaSeguridad){// si ya detectamos que es de seguridad lo enviamos
			next({});
		}else{//revisamos si ahora si hay algo de temas de seguridad
			session.sendTyping();
			var url='http://' + ecibot.SERVER + '/LUIS';
			var datos={ __method__: "GET", api:ecibot.getLUISAPI(),key:ecibot.getLUISKEY(),q: session.userData.situacionEmergencia};
			var callBack=function(J){
				console.log(J.entities);
				var seguridad = builder.EntityRecognizer.findEntity(J.entities, 'Emergencia.seguridad');
				session.userData.situacionEmergenciaSeguridad=seguridad?true:false;//detectamos si es de seguridad o no
				next({});
			};
			ecibotAjaxJson.enviar(url,datos,callBack,session);
		}
	},

	/**
	 * Actuar y enviar el mensaje de emergencia al servidor
	 */
	function (session, args, next) {
		session.send('🚨 Estoy reportando la emergencia 🚨');
		session.sendTyping();
		console.log("temaaa:");
		console.log((session.userData.situacionEmergenciaSeguridad?"seguridad":"emergencia"));
		var url='http://' + ecibot.SERVER + '/Reporte';
		var datos={ __method__: "PUT", mensaje: session.userData.situacionEmergencia, tipoReporte: (session.userData.situacionEmergenciaSeguridad?"seguridad":"emergencia") };
		
		var callBack=function(J){
			var card = new builder.HeroCard(session)
				.title("📢Línea de Emergencia 🚨🚨")
				.subtitle("Escuela Colombiana de Ingeniería")
				.text("")
				.images([
					builder.CardImage.create(session, "https://2.bp.blogspot.com/-8G-nXFnzwbk/WW6ABx82ftI/AAAAAAAA-Z8/qPmqn43TIUIDqHY9xs1CN-ihYDhi4dXDwCLcBGAs/s1600/ambulancia.jpg")
				])
				.buttons([
					builder.CardAction.call(session, "tel:+570316683600", '🚨Llamar🚨'),
				]);
			// Adjuntamos la tarjeta al mensaje
			var msj = new builder.Message(session).addAttachment(card);
			session.send("💡 También puedes llamar a nuestra línea de emergencia!");
			session.send(msj);
			session.endConversation(J["mj"]);
		};

		ecibotAjaxJson.enviar(url,datos,callBack,session);
	}
]