var builder = require('botbuilder');
var request = require('ajax-request');
var dotenv = require('dotenv');

// Para utilizar variables de entorno
dotenv.config();

module.exports = [
	function(session, args, next) {
		// KSSPSCORE:
		// Hacer peticion de uso con metrica HORAR
		if (!args) args = { intent: { entities: {} } };
		// Buscar entidades
        var materia = builder.EntityRecognizer.findEntity(args.entities, 'materia');
        var grupo = builder.EntityRecognizer.findEntity(args.entities, 'grupo');
		//borramos la copia 
		session.dialogData.materia = null;
		session.dialogData.grupo = null;
		session.sendTyping();
		setTimeout(function () {
			if (materia) {
				session.send('📚 Buscar Horario de Asignaturas');
				if (grupo){
					session.dialogData.materia = materia.entity.toUpperCase();
					next({ response: materia.entity, grupo: grupo.entity });
				}else{
					session.dialogData.materia = materia.entity.toUpperCase();
					//builder.Prompts.number(session, "¿Cuál es el grupo de " + materia.entity.toUpperCase() + "📚📚? (Coloca 0 si no lo sabes)");
					next({response: materia.entity});
				}
			} else {
				builder.Prompts.text(session, 'Dame la sigla de la materia 😉');
			}
		}, 2000);
		
	},

	/**
	 * Validar si viene con grupo o sino pedirlo
	 */
	function(session, results, next) {
		if (results.grupo){
			session.dialogData.grupo = results.grupo;
			next();
		}else{
			var materia = results.response.toUpperCase();
			session.dialogData.materia = materia;
			builder.Prompts.number(session, "¿Cuál es el grupo de " + session.dialogData.materia + "? (Coloca 0 si no lo sabes)");
			//console.log("# ----------------- > Aquiii pediii grupo: ");
		}
	},

	/**
	 * Validar grupo
	 */
	function (session, results, next) {
		if (results.response >= 0){
			session.dialogData.grupo = results.response;
			//console.log("# ----------------- > este es el grupo: " + results.response);
			next();
		}else{
			// Ya viene con grupo
			next();
		}
	},

	/**
	 * Enviar mensaje previo y validar numero de grupo
	 */
	function (session, results, next){
		session.sendTyping();
		session.send('Dame un momento, ya estoy buscando 🔍📚');
		var cardGif = new builder.HeroCard(session)
			.images([
				builder.CardImage.create(session, "https://media.giphy.com/media/3o7bufkPz3LRof205G/giphy.gif")
			]);
	
		// Adjuntamos la tarjeta al mensaje
		var msj = new builder.Message(session).addAttachment(cardGif);
		session.send(msj);
		
		var url = "";
		// Validar la sigla
		if (session.dialogData.grupo == 0){
			//console.log("# ----------------- > " + "No viene con grupo");
			url = process.env.SERVER_HORARIO + session.dialogData.materia;
		}else{
			url = process.env.SERVER_HORARIO + session.dialogData.materia + '/' + session.dialogData.grupo;
		}
		//console.log('# ----------------- > ' + url);
		next({ response: url });
	},

	/**
	 * Hacer request a EciUtils
	 */
	function (session, results, next) {
		request(results.response, function (err, res, body) {
			//si no hay conexión con el servidor
			if (body == undefined) {
				session.endConversation("Parece que Kevin no ha configurado bien la conexión con el servidor de horarios, porque no me puedo conectar.");
				return false;
			}
			if (body == "null") {
				session.send("Upsss😭🙄. No encontre nada. ");
				session.send("Intentémoslo de nuevo 😇");
				session.beginDialog("Horario");
				return false;
			}

			//verificamos que la respuesta del servidor sea JSON
			try {
				var J = eval("(" + body + ")");
			} catch (e) {
				var J = Array("res");
				J["error"] = true;
			}

			if (J["error"])
				session.endConversation("Ha ocurrido un error con la conexión: " + body);
			else if (J.length == 0){
				session.send("Upsss😭🙄.. No encontre nada, verifica el grupo y/o la materia y vuelve a intentarlo.");
				next();
			}
			else {
                //guardamos una copia para poder seguir usando
                var listaJson = J;
				var busqueda_resultados = Array();
				// OJO KSSP FB no soporta anotaciones Markdown
				if (session.message.source === "facebook"){
					listaJson.forEach( function(obj) {
						session.send("👉 🔢Grupo: " + obj.Grupo + " | 📅Dia: " + obj.Dia + " | ⏰Horario: " + obj.Inicio  + "-" + obj.Finalizacion + " | 🏢Salón: " + obj.Salon + " | 👨🏻‍🏫Profesor: " + obj.Profesor + " |");
						//busqueda_resultados.push("🔢Grupo: " + obj.Grupo + " 📅Dia: " + obj.Dia + " ⏰Horario: " + obj.Inicio  + "-" + obj.Finalizacion + " 🏢 Salón: " + obj.Salon + "");
					})
				}else{
					listaJson.forEach( function(obj) {
						session.send("* 👉 🔢**Grupo**: " + obj.Grupo + " | 📅**Dia**: " + obj.Dia + " | ⏰**Horario**: " + obj.Inicio  + "-" + obj.Finalizacion + " | 🏢**Salón**: " + obj.Salon + " | 👨🏻‍🏫**Profesor**: " + obj.Profesor + " |");
						//busqueda_resultados.push("🔢Grupo: " + obj.Grupo + " 📅Dia: " + obj.Dia + " ⏰Horario: " + obj.Inicio  + "-" + obj.Finalizacion + " 🏢 Salón: " + obj.Salon + "");
					})
				}

                next();
                //builder.Prompts.choice(session, "Encontre lo siguiente: ", busqueda_resultados.join("|"), { listStyle: 4 });
			}
		});
	},
	
	/**
	 * Preguntar para solicitar otra materia
	 */
	function (session, results, next) {
		builder.Prompts.confirm(session, "¿Necesitas información de otra materia? 🤔");
    },
    
	/**
	 * Redirigir de nuevo a horario
	 */
	function (session, results) {
		if (results.response) {
			session.beginDialog("Horario");
		} else {
			session.endConversation("Recuerda escribir 'MENU' para volver al inicio 👋");
		}
	}

]