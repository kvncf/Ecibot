var builder = require('botbuilder');
var dotenv = require('dotenv');
var ecibot = require('../extra/ecibot');
var ecibotAjaxJson = require('../extra/ecibotAjaxJson');

// Para utilizar variables de entorno
dotenv.config();

module.exports = [
	function(session, args, next) {
		if (!args) args = { intent: { entities: {} } };
		// Buscar entidades
		var conQuien = builder.EntityRecognizer.findEntity(args.entities, 'TelefonoDe');
		//borramos la copia 
		session.userData.ComunicarOpciones = null;
		session.sendTyping();
		setTimeout(function () {
			if (conQuien) {
				session.send('📞 Directorio Administrativo');
				next({ response: conQuien.entity });
			} else {
				builder.Prompts.text(session,'Cuéntame, ¿con quién te quieres comunicar? 🤔');
			}
		}, 3000);
		
	},

	/**
	 * Buscando hacia servidor de la escuela
	 */
	function(session, results, next) {
		var persona = results.response.toUpperCase();
		session.sendTyping();
		session.send('Con mucho gusto, ya mismo estoy buscando...🔍🔍 la información de contacto!');
		next({ response: persona });
	},

	/**
	 * Hacer request a EciBot
	 */
	function (session, results, next) {
		var url = 'http://' + ecibot.SERVER + '/Telefono';
		var datos = { __method__: "GET", nombre:  results.response};
		var callBack = function (J) {
			//guardamos una copia para poder seguir usando
			if (J["resultados"]){
				session.userData.ComunicarOpciones = J["resultados"]["resultado"];
				if (J["resultados"]["resultado"].length == 0) {//si no hay resultados mostramos el mensaje del servidor
					session.send(J["mj"]);
				} else if (J["resultados"]["resultado"].length == 1) {//si no hay resultados mostramos el mensaje del servidor
					next({ response: { index: 0 } });
				} else {//si hay resultados mostramos las opciones
					var encontrados = Array();
					J["resultados"]["resultado"].map(function (e) {encontrados.push(e.nom + " ~ " + e.dep + "");});
					builder.Prompts.choice(session, "Encontré lo siguiente, 🤔 ¿Cuál necesitas?:", encontrados.join("|"), { listStyle: 4 });
				}
			}
		};
		ecibotAjaxJson.enviar(url, datos, callBack,session);
	},

	/**
	 * mostramos el teléfono y damos la opción de llamar al usuario, mediante un link
	 */
	function (session, results, next) {
		console.log(results);
		var opcionSeleccionada = session.userData.ComunicarOpciones[results.response.index];
		var urlMail = "mailto:" + opcionSeleccionada["email"];
		var card = new builder.HeroCard(session)
                .title(opcionSeleccionada["nom"])
                .subtitle(opcionSeleccionada["dep"])
                .text("Extensión: " + opcionSeleccionada["extension"])
                .images([
                    builder.CardImage.create(session, "https://www.xcallymotion.com/assets/img/features/call-center-realtime-monitoring.png")
                ])
                .buttons([
					builder.CardAction.call(session, "tel:+570316683600" , 'Llamar'),
				]);
		// Adjuntamos la tarjeta al mensaje
		var msj = new builder.Message(session).addAttachment(card);
		session.send("👉 Recuerda marcar el número de la extensión 💡");
		session.send(msj);
		session.send("o también contáctalo por correo electrónico: ");
		session.send(urlMail);
		next();
	},

	/**
	 * Preguntar para solicitar otra materia
	 */
	function (session, results, next) {
		builder.Prompts.confirm(session, "🤔 ¿Necesitas otro número de contacto?");
	},
	
	/**
	 * pregunta si quiere comunicarse con alguien más
	 */
	function (session, results) {
		if (results.response) {
			session.beginDialog("Comunicar");
		} else {
			session.endConversation("Recuerda escribir 'MENU' para volver al inicio 👋");
		}
	}

]