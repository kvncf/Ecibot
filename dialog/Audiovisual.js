var builder = require('botbuilder');
var dotenv = require('dotenv');
var ecibot = require('../extra/ecibot');
var ecibotAjaxJson = require('../extra/ecibotAjaxJson');

module.exports = [
	/**
	 * recibe el aviso de ayuda pero intenta capturar el mensaje
	 */
	function (session, args, next) {
		// Buscar entidades
		var accion = builder.EntityRecognizer.findEntity(args.entities, 'AudioVisual.Accion');
		var ayuda = builder.EntityRecognizer.findEntity(args.entities, 'AudioVisual.AyudaCon');
		var lugar = builder.EntityRecognizer.findEntity(args.entities, 'Salon');

		// TEMPORAL
		if (!(accion)) {
			accion = "";
		}

		if (!(ayuda)) {
			ayuda = "";
		}

		if (!(lugar)) {
			lugar = "";
		}

		accion = accion.entity + " " + ayuda.entity;

		session.dialogData.accion = null;
		session.dialogData.lugar = null;

		session.sendTyping();
		if (accion != "undefined undefined") {
			session.send('🔌 Inicio de notificación para Audiovisuales 🔌');
			if (lugar != "") {
				session.dialogData.accion = accion.toUpperCase();
				next({ response: accion, lugar: lugar.entity });
			} else {
				session.dialogData.accion = accion.toUpperCase();
				//builder.Prompts.text(session, 'Dime el salón o el lugar donde se presenta el incidente📚, por favor 😉');
				next({ response: accion });
			}
		} else {
			builder.Prompts.text(session, 'Descríbeme lo que necesitas? 🤔');
		}
	},

    /**
	 * Validar si viene con lugar o sino pedirlo
	 */
	function (session, results, next) {

		if (results.lugar) {
			session.dialogData.lugar = results.lugar.toUpperCase();
			next();
		} else {
			var accion = results.response.toUpperCase();
			session.dialogData.accion = accion;
			builder.Prompts.text(session, 'Dime el salón o el lugar donde se presenta el incidente, por favor 😉');
		}
	},

    /**
	 * Validar lugar
	 */
	function (session, results, next) {
		if (results.response) {
			session.dialogData.lugar = results.response.toUpperCase();
			next();
		} else {
			// Ya viene con lugar
			next();
		}
	},

    /**
	 * Enviar mensaje previo
	 */
	function (session, results, next) {
		session.sendTyping();
		session.send('Con mucho gusto😉, ya mismo estoy reportando el incidente al area de Audiovisuales🔍🧐');
		next();
	},

	/**
	 * Actuar y enviar el mensaje de audiovisuales al servidor
	 */
	function (session, args) {
		var url = 'http://' + ecibot.SERVER + '/Reporte';
		var datos = { __method__: "PUT", mensaje: session.dialogData.accion + ' lugar:' + session.dialogData.lugar, tipoReporte: "audiovisuales" };
		var callBack = function (J) {
			var card = new builder.HeroCard(session)
				.title("📢Línea de Audiovisuales")
				.subtitle("Escuela Colombiana de Ingeniería")
				.text("")
				.images([
					builder.CardImage.create(session, "http://jimbra.com/images/500_300.png")
				])
				.buttons([
					builder.CardAction.call(session, "tel:+570" + ecibot.TEL_AUDIOVISUALES, 'Llamar'),
				]);
			// Adjuntamos la tarjeta al mensaje
			var msj = new builder.Message(session).addAttachment(card);
			session.send(msj);
			session.endConversation(J["mj"]);
		};
		ecibotAjaxJson.enviar(url, datos, callBack, session);
	}
]