var builder = require('botbuilder');
var dotenv = require('dotenv');

module.exports = [
    function (session) { 
        var cardGif = new builder.HeroCard(session)
            .images([
                builder.CardImage.create(session, "https://media.giphy.com/media/l0NwsrAFr3czWgC0E/giphy.gif")
            ]);

        // Adjuntamos la tarjeta al mensaje
        var msj = new builder.Message(session).addAttachment(cardGif);
        session.send(msj);
        if(session.userData.nombre){
            session.endConversation(`Disculpa ${session.userData.nombre}, no entiendo lo que deseas. Recuerda que soy un robot. Escribe "Menu" y te mostraré los servicios en que puedo ayudarte`);
        }else{
            session.endConversation(`Disculpa, no entiendo lo que deseas. Recuerda que soy un robot. Escribe "Menu" y te mostraré los servicios en que puedo ayudarte`);
        }
        
    }
]