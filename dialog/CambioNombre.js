var builder = require('botbuilder');
var dotenv = require('dotenv');

// Para utilizar variables de entorno
dotenv.config();

module.exports = [
	function(session, args, next) {
		if (!args) args = { intent: { entities: {} } };
		// Buscar entidades
		var username = builder.EntityRecognizer.findEntity(args.entities, 'NameSession');
		session.sendTyping();
		setTimeout(function () {
			if (username) {
				next({ response: username.entity });
			} else {
				builder.Prompts.text(session,'Dime, ¿Cómo quieres que te llame? 🤔');
			}
		}, 3000);
		
	},

	/**
	 * Corrigiendo nombre
	 */
	function(session, results, next) {
		var nombreUsuario = results.response.toUpperCase();
        session.sendTyping();
		session.send('Con mucho gusto, ya estoy corrigiendo tú nombre!');
        session.userData.nombre = nombreUsuario;
        session.endConversation(`Ya corregí tú nombre ${session.userData.nombre} 😄`)
	},

]