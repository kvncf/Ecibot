module.exports = {
	getDialogo:function(parametros) { 		var builder=parametros._builder; 		var ecibot=parametros._ecibot; 		var request=parametros._request;
		return [
			/**
			 * pregunta el nivel de grado
			 */
			function (session, args, next) {
				var niveles = ecibot.opcionesNivelGrado();
				builder.Prompts.choice(session, "La admisión es para:", niveles.join("|"), { listStyle: 4 });
			},
			/**
			 * retorna la respuesta del usuario y lo guarda en userData
			 */
			function (session, results) {
				session.userData.nivelGrado = results.response.entity;
				session.endDialogWithResult(results);
			}
		]
	}
}