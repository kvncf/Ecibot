var builder = require('botbuilder');
var request = require('ajax-request');
var dotenv = require('dotenv');

// Para utilizar variables de entorno
dotenv.config();

module.exports = [
	function(session, args, next) {
		// KSSPSCORE:
		// Hacer peticion de uso con metrica CLIM
		session.sendTyping();
		setTimeout(function () {
            session.send('📚 Fuente OpenWeatherMap.org');
            next();
		}, 2000);
		
	},

	/**
	 * Enviar mensaje
	 */
	function (session, results, next){
        var url = "";
        session.sendTyping();

        session.send('Ya mismo estoy buscando, esto tomará un tiempo 🔍');
        var cardGif = new builder.HeroCard(session)
            .images([
                builder.CardImage.create(session, "https://media.giphy.com/media/d1E1pZ1cdgWmY0hy/giphy.gif")
            ]);

        // Adjuntamos la tarjeta al mensaje
        var msj = new builder.Message(session).addAttachment(cardGif);
        session.send(msj);

        url = process.env.SERVER_CLIMA;
        //console.log('# ----------------- > ' + url);
        next({ response: url });
	},

	/**
	 * Hacer request a ApiWeatherMap
	 */
	function (session, results, next) {
		request(results.response, function (err, res, body) {
			//si no hay conexión con el servidor
			if (body == undefined) {
				session.endConversation("Parece que Kevin no ha configurado bien la conexión con el servidor de noticias, porque no me puedo conectar.");
				return false;
			}
			if (body == "null") {
				session.send("Upsss😭🙄. No encontre nada. ");
				session.send("Intentémoslo de nuevo 😇");
				return false;
			}

			//verificamos que la respuesta del servidor sea JSON
			try {
				var J = eval("(" + body + ")");
			} catch (e) {
				var J = Array("res");
				J["error"] = true;
			}

			if (J["error"])
				session.endConversation("Ha ocurrido un error con la conexión: " + body);
			else if (J.length == 0){
				session.send("Upsss😭🙄.. No encontre nada, verifica el grupo y/o la materia y vuelve a intentarlo.");
				next();
			}
			else {
                var card = new builder.ThumbnailCard(session)
                    .title(J.main.temp + "°C" + ", " + J.weather[0].description.toUpperCase())
                    .subtitle("Escuela Colombiana de Ingeniería Julio Garavito, BOG-CO")
                    .text("Min. " + J.main.temp_min + "°C - Max. " + J.main.temp_max + "°C")
                    .images([
                        builder.CardImage.create(session, "http://openweathermap.org/themes/openweathermap/assets/vendor/owm/img/widgets/" + J.weather[0].icon + ".png")
                    ])

                // Adjuntamos la tarjeta al mensaje
                var msj = new builder.Message(session).addAttachment(card);
                session.endConversation(msj);
			}
		});
	},

]