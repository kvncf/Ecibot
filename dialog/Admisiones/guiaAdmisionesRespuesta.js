module.exports = {
	getDialogo:function(parametros) { 		var builder=parametros._builder; 		var ecibot=parametros._ecibot; 		var request=parametros._request;
		return [
			/**
			 * Mostramos la información de cómo va el proceso de matrícula
			 */
			function (session, args, next) {
				if (ecibot.esNivel(ecibot.PREGRADO, session.userData.nivelGrado)) {
					session.endDialog(ecibot.admision.getRespuesta(ecibot.PREGRADO, false));
				} else {
					session.send(ecibot.admision.getRespuesta(ecibot.POSGRADO, false));
					session.beginDialog("preguntarSiEsExtranjero");
				}
			},
			/**
			 * si es extrangero muestra la información correspondiente
			 */
			function (session, results) {
				if (results.response) {
					session.send(ecibot.admision.getRespuesta(ecibot.POSGRADO, true));
					session.endDialog("y eso es todo por ahora.");
				} else {
					session.endDialog("vale, por ahora eso es todo.");
				}
			}
		]
	}
}