module.exports = {
	getDialogo:function(parametros) { 		var builder=parametros._builder; 		var ecibot=parametros._ecibot; 		var request=parametros._request;
		return [
			/**
			 * muestra los requisitos y pregunta si es extrangero
			 */
			function (session, args, next) {
				
				if (ecibot.esNivel(ecibot.PREGRADO, session.userData.nivelGrado)) {
					session.send(ecibot.admision.getRequisitos(ecibot.PREGRADO,"minimo"));
					session.send(
						new builder.Message(session).addAttachment(
							new builder.HeroCard(session).buttons([
								builder.CardAction.openUrl(session, ecibot.admision.getRequisitos(ecibot.PREGRADO,"url") , 'Leer más')
							])
						)
					);
				} else {
					session.send(ecibot.admision.getRequisitos(ecibot.POSGRADO,"minimo"));
				}
				session.beginDialog("preguntarSiEsExtranjero");
			},
			/**
			 * si es extrangero muestra la información correspondiente
			 */
			function (session, results) {
				if (results.response) {
					if (ecibot.esNivel(ecibot.PREGRADO, session.userData.nivelGrado)){
						session.send(ecibot.admision.getRequisitos(ecibot.PREGRADO, "extranjero"));
						session.send(
							new builder.Message(session).addAttachment(
								new builder.HeroCard(session).buttons([
									builder.CardAction.openUrl(session, ecibot.admision.getRequisitos(ecibot.PREGRADO,"url2") , 'Leer más')
								])
							)
						);
					}else{
						session.send(ecibot.admision.getRequisitos(ecibot.POSGRADO, "extranjero"));
						session.send(
							new builder.Message(session).addAttachment(
								new builder.HeroCard(session).buttons([
									builder.CardAction.openUrl(session, ecibot.admision.getRequisitos(ecibot.POSGRADO,"url2") , 'Leer más')
								])
							)
						);
					}
					session.endDialog();
				} else {
					session.endDialog("vale, por ahora eso es todo.");
				}
			}
		]
	}
}