var builder = require('botbuilder');
var dotenv = require('dotenv');
var ecibot = require('../extra/ecibot');
var ecibotAjaxJson = require('../extra/ecibotAjaxJson');

module.exports = [
	/**
	 * Escribir comentario
	 */
	function (session, args, next) {
		// KSSPSCORE:
		// Hacer peticion de uso con metrica SUGER
		// Buscar entidades
		var sugerencia = args.entities;
		session.dialogData.comentario = "";
		session.sendTyping();
		setTimeout(function () {
			if (sugerencia) {
				session.send('Comentarios o Sugerencias 👎🏻👍🏻');
				builder.Prompts.text(session, 'Genial! 💡, cuéntame 🤔 ¿Qué comentario tienes?');
			} else {
				builder.Prompts.text(session, 'Genial! 💡, cuéntame 🤔 ¿Qué comentario tienes?');
			}
		}, 3000);

	},

	/**
	 * Confirmacion
	 */
	function (session, args, next) {
		session.dialogData.comentario = args.response;
		builder.Prompts.confirm(session, '¿Deseas enviar el comentario? 🤔');
	},

	/**
	 * envia el mensaje de sugerencia
	 */
	function (session, args) {
		if (args.response) {
			session.send('Estoy enviando tu comentario 👍');

			var url = 'http://' + ecibot.SERVER + '/Sugerencia';
			var datos = { __method__: "PUT", sugerencia: session.dialogData.comentario };
			var callBack = function (J) {
				var cardGif = new builder.HeroCard(session)
					.images([
						builder.CardImage.create(session, "https://media.giphy.com/media/w77O4Mf1juHPq/giphy.gif")
					]);

				// Adjuntamos la tarjeta al mensaje
				var msj = new builder.Message(session).addAttachment(cardGif);
				session.send(msj);
				session.endConversation(J["mj"] + " tendremos en cuenta tú comentario, gracias 😀");
				session.endConversation("Recuerda escribir 'MENU' para volver al inicio 👋");
			};
			ecibotAjaxJson.enviar(url, datos, callBack, session);

		}
	}
]