/**
 * funcion ajax para peticiones al servidor
 */
var request = require('ajax-request');
module.exports={
	/**
	 * indica si el nivel que pide es el que se tiene de información hasta el momento
	 */
	enviar:function(dirUrl,dataAjax,callback,session) {
		request.post({
			headers: { 'content-type': 'application/x-www-form-urlencoded' },
			url: dirUrl,
			data: dataAjax
		}, function (error, response, body) {
			//si no hay conexión con el servidor
			if (body == undefined) {
				session.endDialog("Parece que Kevin no ha configurado bien la conexión con el servidor, porque no me puedo conectar.");
				return false;
			}

			//verificamos que la respuesta del servidor sea JSON
			try {
				var J = eval("(" + body + ")");
			} catch (e) {
				var J = Array("res");
				J["error"] = true;
				J["mj"] = body;
			}
			if (J["error"]) session.endConversation("Ha ocurrido un error con la conexión: **" + J["mj"] + "**");
			callback(J);
		});
	}
};