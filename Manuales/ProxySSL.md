# **Configuración de Proxy SSL**

## **Generando certificados para Slackware**
### **Generando certificados**

1. Creamos la llave privada

		openssl genrsa -out server.key 1024

2. Creamos un certificado de Registro de Firma CSR

		openssl req -new -key server.key -out server.csr

	Ingresar datos personales de la entidad

		Codigo País:CO 
		Departamento: Bogota
		Ciudad: Bogota
		Nombre Empresa: Escuela Colombiana de Ingenieria
		Tipo de Organizacion: Education
		Nombre: Nombre personal
		Email:email personal
		opcional el password

3. Generamos el certificado SSL

	Este paso se puede realizar cada año al finalizar el certificado

		openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

	Confirmar que tengamos los archivos

		ls
		-> server.key
		-> server.crt
		-> server.csr

4. copiamos los certificados en apache
	
		sudo cp server.crt /usr/local/apache/conf/
		sudo cp server.key /usr/local/apache/conf/

### **Configuración de archivos Apache**

1. Habilitar los módulos necesarios

	Nos ubicamos en configuración de apache

		cd /usr/local/apache/conf

	Realizamos una copia de seguridad

		cp httpd.conf httpd.conf.copia
		vi httpd.conf

	Habilitamos los módulos descomentando:
		
		LoadModule socache_shmcb_module ...
		LoadModule ssl_module ...

	Habilitamos la lectura al archivo *httpd_ssl.conf* descomentando la línea:

		Include conf/extra/httpd-ssl.conf
	
	Cerramos el archivo

2. **Configuramos el archivo SSL**

	Nos ubicamos en configuración de apache

		cd /usr/local/apache/conf/extra

	Realizamos una copia de seguridad

		cp httpd-ssl.conf httpd-ssl.conf.copia
		vi httpd-ssl.conf

	Agregamos las líneas después de la línea : (*SSLCertificatesKeyFile "..."*)

		SSLProxyEngine on
		SSLProxyVerify none
		SSLProxyCheckPeerCN off
		SSLProxyCheckPeerName off
		SSLProxyCheckPeerExpire off
		ProxyRequests off
		ProxyPreserveHost on
	Si se quiere una url personalizada cambiar **ecibot** por la que se desee:

		ProxyPass /ecibot/ https://direccionIP/
		ProxyPassReverse /ecibot/ https://direccionIP/

1. **Reiniciamos apache**

		/usr/local/apache/bin/apachectl stop
		/usr/local/apache/bin/apachectl start

___
## **Generando certificados para Ubuntu Server**

### **Generando certificados**

1. Instalar SSl

		sudo apt-get install openssl

2. Creamos la llave privada

		openssl genrsa -out server.key 1024

3. Creamos un certificado de Registro de Firma CSR

		openssl req -new -key server.key -out server.csr

4. Generamos el certificado SSL

		openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

1. Copiamos los certificados en apache
	
		sudo cp server.crt /etc/ssl/certs/ssl.crt
		sudo cp server.key /etc/ssl/certs/ssl.key

6. Habilitamos el módulo SSL

		sudo a2enmod ssl

### **Configuración de Proxy HTTPS**

1. Elegimos el archivo para configuración de proxy ssl

		sudo vi /etc/apache2/sites-available/frontend.ssl.conf

1. Modificamos el archivo con las siguientes líneas
		
		ProxyRequests Off
		ProxyPreserveHost On
		
		SSLEngine on
		
		ProxyPass / https://servidorAdirigir/
		ProxyPassReverse / https://servidorAdirigir/
		
		SSLCertificateFile /etc/ssl/certs/ssl.crt
		SSLCertificateKeyFile /etc/ssl/certs/ssl.key

1. Agregamos el archivo a los sitios agregados de apache

		sudo a2ensite /etc/apache2/sites-available/frontend.ssl.conf
		sudo service apache2 restart