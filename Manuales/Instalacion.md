# **Manual de instalación y requerimientos**

## **Requerimientos**

* Máquina virtual con:
	* Características:
		* Memoria RAM: 2 Gb
		* Disponible en Disco duro: 10Gb
		* Arquitectura de Procesador: 32Bits
		* Sistemas Operativo: Ubuntu Server v16.04
	* Tecnologías:
		* Apache v2
		* PHP v7.0
		* NodeJS
* Computador para desarrollo:
	* Características
		* Memoria RAM: 4 Gb
		* Disponible en Disco duro: 10Gb
		* Arquitectura de Procesador: 64Bits
		* Procesador: 1.2GHz
		* Sistemas Operativo: Windows 10 64Bits
	* Programas:
		* NodeJS
		* IDE (sugerencia Visual Studio Code)
		* Git
		* Microsoft Bot Emulator [Microsoft Bot Emulator](https://github.com/Microsoft/BotFramework-Emulator)
		* Ngrok

* Crear cuentas en:
	* Microsoft Azure
	* LUIS
	* QnA

## **Actualmente instalado**

* Máquina virtual Ubuntu Server:
	* Se deja instalada en el laboratorio de informática de la universidad, con una ip asignada de: *10.2.65.25* se mantienen activos el chatbot en el puerto: *3978* y apache se mantiene asginado el puerto *80* para el acceso al servidor se tienen asignadas las siguientes credenciales:

			Usuario: ecibot
			Clave: ecibot

* Proxy Laboratorio de informática (Configuración):
	* De acceso externo se tiene  *chatbot.is.escuelaing.edu.co:80* el cuál se encuentra direccionado a *10.2.65.25:80* 
	* Se ha configurado el protocolo HTTPS permitiendo acceder desde *ecbot-is.escuelaing.edu.co:443* el cuál se encuentra direccionado a *10.2.65.25:3978* 
* Administrador de reportes (3 computadores a futuro):
	* Accediendo a través del navegador a la ruta [chatbot.is.escuelaing.edu.co/admin](http://chatbot.is.escuelaing.edu.co/admin)
	* Las cuentas de acceso serán asignadas de forma personal
---
# **¿Cómo instalar?**

## **Servidor**

1. Descargar el sistema operativo e instalar en la máquina virtual de características cómo se indicó anteriormente.

	### **IMPORTANTE**: no olvidar el usuario de configuración para el servidor con el nombre:

		ecibot

	con la idea que permita configurarse de forma correcta como se mostrará en las próximas instrucciones

2. Realizar el proceso de instalación inicial sin problema hasta ver el siguiente pantallazo:

	![](Map/seleccion_de_instalacion_ubuntu_server.jpg)
	
3. si ya ha llegado al pantallazo que se muestra anteriormente debe activar adicionalmente **LAMP Server** y **OpenSSH Server**

	cuando finalmente tenga acceso al servidor puede continuar con la instalación del chatbot en él, cómo se muestra en la siguiente instrucción

	### **Configuración PHP**
	1. Acceder a el archivo de configuración de php:

			sudo vi /etc/php/7.0/apache2/php.ini

	1. Modificar la línea **short_open_tag = Off** por:

			short_open_tag = On
			
	1. Ajustando hora y fecha de timezone, modificar la línea **date.timezone =** por:

			date.timezone = "America/Bogota";
		cerrar el archivo

	1. comando para instalar xml extension

			sudo apt-get install php-xml

	1. Reiniciamos el servicio de apache

			sudo service apache2 restart

	### **Habilitar ModRewrite**

	1. Activar el módulo mediante la instrucción:

			sudo a2enmod rewrite

	1. Editar archivo para agregar las lineas para redirección de uri

			sudo vi /etc/apache2/apache.conf

	1. En las líneas de configuración de la ruta del sitio modificar la línea:

			AllowOverride All

	4. Reiniciamos el servicio

			sudo service apache2 restart

	### **VPN (Opcional, para acceso remoto externo)**

	1. Instalar OpenVPN

			sudo apt-get install openvpn
	2. Descargar los archivos de la ruta

			./VPN/Servidor

	1. Probar la configuración de conexión VPN de la siguiente forma:

			openvpn --config VPN/Servidor/chatbot-server.ovpn

	1. Al tenerlo funcionando se debe dejar en el arranque así que movemos todos los archivos a la ruta:

			/etc/openvpn/

	1. Renombramos el archivo:

			mv chatbot-server.ovpn chatbot-server.conf

	1. Reiniciar y probar con **ifconfig** que ya esté funcionando la nueva tarjeta de red VPN

	### **NodeJS**
	1. Instalar **NodeJS**

			sudo apt-get update
			sudo apt-get install nodejs
			sudo apt-get install npm
	
	### **Forever** (para la versión definitiva)

	Se debe instalar **forever** para que el proceso de node **app.js** siga ejecutándose, para esto usamos:

	1. instalar

        	npm install forever --global

	1. Podemos probar su funcionamiento con:

			forever start app.js

	1. Podemos ver el listado de procesos andando con:

			forever list

	1. Podemos ver la consola de dicha aplicación, dónde **0** es el id de la app, con:

			forever logs 0

	5. Cómo nuestro interés es que el proceso quede funcionando aún después de reiniciar el servidor, entonces generamos un cronjob de esta forma:

			sudo crontab -e
			2

	1. Agregamos la línea al final del archivo y guardamos:

			@reboot /usr/local/bin/forever start /var/www/nodes/forever.json > /dev/null 2>&1

	7. Reiniciar el servidor y sin necesidad de ejecutar de nuevo nodejs veremos que la aplicación js debe seguir funcionando.


	### **Apache**

	1. editar el sitio de acceso al puerto 80

			sudo vi /etc/apache2/sites-available/000-default.conf

	2. Editar el virtualhost que debe quedar así:

			<VirtualHost *:80>
				DocumentRoot /home/ecibot/ChatbotNodeJS
				ServerAdmin kevin.alvarado@mail.escuelaing.edu.co
				ErrorLog ${APACHE_LOG_DIR}/error.log
				CustomLog ${APACHE_LOG_DIR}/access.log combined
			</VirtualHost>
	3. Editamos la configuración de permisos a la carpeta

			sudo vi /etc/apache2/apache2.conf
	1. modifcando las líneas de acceso quedando así:

			<Directory /home/ecibot/ChatbotNodeJS>
				Options Indexes FollowSymLinks MultiViews
				AllowOverride All
				Order allow,deny
				allow from all
			</Directory>

## **Chatbot**

1. Clonar [este](https://gitlab.com/EciChatbot/Ecibot) repositorio en la ruta

		/home/ecibot

3. Iniciar el proyecto (indicar todo que **sí** y asignar los valores que desee)

		npm init

4. Colocar los siguientes comandos para instalar los componentes requeridos:

		npm install

5.  Para iniciar el chatbot usar:

		node app.js

1. La instrucción anterior permite acceder a la ruta:

		http://localhost:3978/api/messages
		http://10.2.65.25:3978/api/messages
		https://ecbot-is.escuelaing.edu.co/api/messages

	la cuál va a permitir ser usada en el emulador para hacer pruebas o Microsoft Azure para la versión definitiva (más adelante se indicará sobre esto)

## **Computador** (acceso administrador de reportes)

1. Abrir el navegador web (preferiblemente Google Chrome)
2. Acceder al sitio:

		chatbot.is.escuelaing.edu.co/admin

3. Usar las credenciales asignadas, en caso de no saberlas comunicarse con el administrador.

## **Computador** (desarrollo)

1. Para el computador de desarrollo se debe instalados los programas indicados en el principio del manual de instalación.
2. Para probar se usará el Microsoft Bot Emulator, que nos va a permitir interactuar con el Chatbot.
3. Configurar el Ngrok
	1. Luego de tener descargado Ngrok, abrir Microsoft Bot Emulator
	2. Arriba a la derecha en los 3 puntos hacer click, dónde se desplegará un menú como se muestra en la imagen, hacer clic en Settings

		![](Map/emulator_ngrok1.PNG)

	3. En la configuración se debe indicar la ruta hacia el archivo de Ngrok.exe descargado anteriormente.

		![](Map/emulator_ngrok1.1.PNG)

	4. Luego de esto nos debe indicar en el log de salida algo similar a, (si no aparece se puede reiniciar el emulador): 

		![](Map/emulator_ngrok2.PNG)

	5. Luego de iniciado el servicio de NodeJS como se ha indicado en los pasos ateriores se procede a ingresar la URL según sea el caso:
		
		A. Si es Local usar:

			http://localhost:3978/api/messages

		B. Si es en la red del laboratorio usar:

			http://10.2.65.25:3978/api/messages

		C. si es en internet o VPN usar:

			https://ecbot-is.escuelaing.edu.co/api/messages

	6. Permitirá acceder luego de autenticar el chatbot, para esto se ingresarán los ID y password que Azure ha proporcionado al registrar el chatbot, que se verá tambien colocado en el archivo

			/.env

		Así:

			# Bot Framework Credentials
			MICROSOFT_APP_ID=ABCD-EFG..
			MICROSOFT_APP_PASSWORD=CLAVEEEEE

	7. como se muestra en la siguiente imagen se deben ingresar las credenciales mencionadas:

		![](Map/emulator_ngrok3.PNG)

	1. Finalizando así la configuración inicial y podrá ser probado el Chatbot.