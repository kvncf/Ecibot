<?
require_once "c.FacilJSON.php";
/**

*/
abstract class absFacilJSON{

	private $fj;
	function __construct() {
		$this->fj=new FacilJSON();
	}
	
	//redefinido de FacilJSON
	protected final function setERROR($newERROR){
		$this->fj->setERROR($newERROR);
	}
	
	//redefinido de FacilJSON
	protected final function getERROR($id){
		return $this->fj->getERROR($id);
	}
	
	//redefinido de FacilJSON
	protected final function ERROR($id){
		$this->fj->ERROR($id);
	}
	
	//redefinido de FacilJSON
	protected final function json($key,$val=NULL){
		return $this->fj->json($key,$val);
	}

	//redefinido de FacilJSON
	protected final function mj($val=NULL){
		return $this->fj->mj($val);
	}
	
	//redefinido de FacilJSON
	protected final function ok(){
		return $this->fj->ok();
	}

	//redefinido de FacilJSON
	protected final function res($val=NULL){
		return $this->fj->res($val);
	}

	//redefinido de FacilJSON
	protected final function falla($mensaje){
		return $this->fj->falla($mensaje);
	}

	//redefinido de FacilJSON
	protected final function send(){
		$this->fj->send();
	}
}
?>