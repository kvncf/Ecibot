<?
/**
permite definir variables para respuesta de peticiones a php, permite controlar y definir errores para una fácil gestión y control
*/
class FacilJSON{
	public $r=array("res"=>true,"mj"=>"mj vacio","error"=>false);
	private $errorArray=array();
	
	/**
	permite obtener o establecer un error del json 
	val: valor puede ser nulo
	*/
	private function error_($val=NULL){
		if($val===false)$this->res(false);
		return $this->json("error",$val);
	}

	/**
	establece los mensajes de error
	newERROR: debe ser un arreglo
	*/
	public final function setERROR($newERROR){
		$this->errorArray=$newERROR;
	}
	
	/**
	retorna el arreglo ERROR o devuelve el mensaje del error según el id
	id: debe ser alguna llave de ERROR
	*/
	public final function getERROR($id){
		if(!isset($id))return $this->errorArray;
		return $this->errorArray[$id];
	}
	
	/**
	define el error id, generando una falla
	id: debe ser alguna llave de ERROR
	*/
	public final function ERROR($id){
		$this->falla($this->errorArray[$id]);
	}
	
	/**
	permite obtener o establecer un valor de una llave del json 
	key: llave
	val: valor puede ser nulo
	*/
	public function json($key,$val=NULL){
		if(isset($val)){//si se asigna valor
			$this->r[$key]=$val;
			return true;
		}else return $this->r[$key];
	}

	/**
	permite obtener o establecer un mj del json 
	val: valor puede ser nulo
	*/
	public final function mj($val=NULL){
		//si hay error se acomulan los mensajes
		if(isset($val) && $this->error_())$val=$this->mj().", ".$val;
		return $this->json("mj",$val);
	}
	
	/**
	retorna el estado de la ejecución, lo inverso a error_();
	*/
	public final function ok(){
		return !$this->error_();
	}

	/**
	permite obtener o establecer un res del json 
	val: valor puede ser nulo
	*/
	public final function res($val=NULL){
		return $this->json("res",$val);
	}
	
	/**
	permite establecer una falla dela ejecución, asignando un mensaje 
	mensaje: mensaje de la falla
	*/
	public final function falla($mensaje){
		$this->mj($mensaje);
		$this->error_(true);
		return true;
	}

	/**
	imprime JSON para envío
	*/
	public final function send(){
		echo json_encode($this->r);
	}

	/**
	Se ejecuta antes de el GET, POST, DELETE y PUT
	*/
	public function beforeExecute(){
	}

	/**
	Método que ejecuta la petición GET
	*/
	public function get(){
		die($this->errorNoPermitido);
	}
	/**
	Método que ejecuta la petición DELETE
	*/
	public function delete(){
		die($this->errorNoPermitido);
	}
	/**
	Método que ejecuta la petición POST
	*/
	public function post(){
		die($this->errorNoPermitido);
	}
	/**
	Método que ejecuta la petición PUT
	*/
	public function put(){
		die($this->errorNoPermitido);
	}
}
?>