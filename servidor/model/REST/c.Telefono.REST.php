<?
error_reporting(E_ERROR | E_PARSE);
require_once "../c.REST.php";
require_once "../xml2json.php";

class Telefono extends REST{

	function get(){
		//buscamos la url de la escuela
		$homepage = file_get_contents('http://tycho.escuelaing.edu.co/directorio/BuscarEci?param='.$this->DATA["nombre"]);
		//comvertimos el xml a json
		$jsonContents = xml2json::transformXmlStringToJson($homepage);
		//comvertimos a array
		$h=json_decode($jsonContents,true);
		
		//verificamos que haya encontrado algo
		if($h["resultados"]["resultado"]["email"]=="No se encontraron registros"){
			$h["resultados"]["resultado"]=array();
			$this->mj("Upsss😭🙄. No pude encontrarlo, vuelve a intentar con sólo un nombre o apellido por favor.");
		}elseif(isset($h["resultados"]["resultado"]["email"])){
			$tmp=$h["resultados"]["resultado"];
			$h["resultados"]["resultado"]=array();
			$h["resultados"]["resultado"][]=$tmp;
		}
		$this->json("resultados",$h["resultados"]);
		$this->send();
	}
}
new Telefono(true);
?>