<?
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', '1');
require_once "../c.REST.php";
require_once "../c.ArchivoJSON.php";

class Reporte extends REST{

	private $archivo;

	function get(){
		$this->revisarCookieTipoReporte();
		$this->revisarTipoReporte();
		$ajson=new ArchivoJSON($this->archivo);
		$this->json("bd",$ajson->leer());
		$this->send();
	}

	function put(){
		$this->revisarTipoReporte();
		$mensaje=$this->DATA["mensaje"];
		if(!$mensaje){
			$this->falla("El mensaje ha llegado vacío.");
			$this->send();
			die();
		}
		$ajson=new ArchivoJSON($this->archivo);
		$bd=$ajson->leer();
		//agregamos el reporte nuevo
		$bd[]=array(
			"id"=>rand(1,100000000),
			"fecha"=>date("d-m-Y h:i a"),
			"atendido"=>false,
			"mensaje"=>base64_encode($mensaje)
		);

		//guardamos el reporte nuevo
		$ajson->escribir($bd);
		$this->mj("Reporte enviado, pronto estarán en contacto contigo.");
		$this->send();
	}

	function post(){
		$this->revisarCookieTipoReporte();
		$this->revisarTipoReporte();

		$id=$this->DATA["id"];
		if(!$id){
			$this->falla("El id ha llegado vacío.");
			$this->send();
			die();
		}
		$ajson=new ArchivoJSON($this->archivo);
		$bd=$ajson->leer();
		//lo marcamos como atendido
		for ($i=0; $i<count($bd); $i++)
			if($bd[$i]["id"]==$id){
				$bd[$i]["atendido"]=true;
				break;
			}

		//guardamos el reporte nuevo
		$ajson->escribir($bd);
		$this->send();
	}

	/**
	
	 */
	private function revisarCookieTipoReporte(){
		//se usa la cookie, porque debe iniciar sesión el usuario
		if(!isset($_COOKIE["entidad"])){
			$this->falla("No tienes permisos para ver el contenido.");
			$this->send();
			die();
		}
		$this->DATA["tipoReporte"]=$_COOKIE["entidad"];
	}

	/**
	basado en el tipoReporte define el archivo de bd, si tipoReporte es vacío termina con falla
	 */
	private function revisarTipoReporte(){
		$tipoReporte=$this->DATA["tipoReporte"];
		if(!$tipoReporte){
			$this->falla("No se ha difinido el tipo de reporte.");
			$this->send();
			die();
		}
		$this->archivo="../../bd/".$tipoReporte.".bd";
	}
}
new Reporte(true);
?>