<?
require_once "../c.REST.php";
require_once "../c.ArchivoJSON.php";

class Sugerencia extends REST{

	private $archivo="../../bd/sugerencias.bd";

	function put(){
		$sugerencia=$this->DATA["sugerencia"];
		if(!$sugerencia){
			$this->falla("El mensaje ha llegado vacío.");
			$this->send();
			die();
		}

		$ajson=new ArchivoJSON($this->archivo);
		$bd=$ajson->leer();
		//agregamos el reporte nuevo
		$bd[]=array(
			"id"=>rand(1,100000000),
			"fecha"=>date("d-m-Y h:i a"),
			"atendido"=>false,
			"mensaje"=>base64_encode($sugerencia)
		);

		//guardamos el reporte nuevo
		$ajson->escribir($bd);
		$this->mj("Guardada!.");
		$this->send();
	}
}
new Sugerencia(true);
?>