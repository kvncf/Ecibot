var Ajax_ = function (settings) {
	/*
	url
	type
	data
	funcion
	selfCallback
	selfCallbackVars
	json
	callback
	*/
	$.ajax({
		url: settings.url,
		type: (settings.type == "PUT" || settings.type == "DELETE") ? "POST" : settings.type,
		data: (settings.type == "GET"?"":"__method__=" + settings.type + "&")+ settings.data
	}).fail(function (jqXHR, textStatus) {
		if (FReTry(settings.funcion + settings.type, textStatus) === false) setTimeout(function () { settings.selfCallback(settings.selfCallbackVars); }, 1000);
	}).done(function (msg) {
		var J = msg;
		if (settings.json) {
			J = Array();
			try {
				J = eval("(" + msg + ")");
			} catch (/*~PF-*/e/*~PF+*/) {
				J = Array("res", "mj");
				J["mj"] = msg;
				J["res"] = false;
				J["error"] = true;
			}
			if (J["error"]) {
				J["mj"] = "Ocurri&oacute; el siguiente error inf&oacute;rmalo: <br><br>" + J["mj"]; J["res"] = false;
			}
		}
		settings.callback(J);
	});
};