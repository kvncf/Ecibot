var reporte = (function () {
	var x = {
		url: "Reporte",
		funcion: "reporte"
	};
	var total = -1;
	var titulo = "Listado de Reportes";
	var urlAdmin = "/admin";
	var iniciarAlerta = function (J) {
		$("#alerta").html("<div><span>" + J.fecha + "</span> " + Base64.decode(J.mensaje) + "<br><input type='button' onclick='reporte.apagarAlerta(" + J.id + ");' value='Atender'><audio controls autoplay><source src='I/"+$("#entidad").text()+".mp3' type='audio/mp3'></audio></div>").show(500);
	}

	return {
		/**
		 * función que se ejecuta al iniciar el sitio
		 */
		init: function () {
			setTimeout(reporte.RevisarCookie, 200);
		},
		/**
		 * revisa si el usuario ya inició sesión
		 */
		RevisarCookie: function (params) {
			MJ_load(true);
			//creamos otra variable para NO usar la global
			var z = {
				url: "Admin",
				funcion: x.funcion
			};
			z.type = "GET";
			z.data = "";
			z.selfCallback = reporte.GET;
			z.selfCallbackVars = params;
			z.json = true;
			z.callback = function (J) {
				if (J["error"]) {//si hay error
					MJ_simple("Revisando acceso admin", J["mj"]);
				}else if (!J["res"]) {// si no inició
					location.href=urlAdmin;
				}else{//si ya inició
					$("#entidad").text(J["entidad"]);
					setTimeout(function(){MJ_load(false);},100);
					reporte.GET();
				}
			};
			Ajax_(z);
		},
		/**
		 * Cerrar sesión
		 */
		Salir: function (params) {
			MJ_load(true);
			//creamos otra variable para NO usar la global
			var z = {
				url: "Admin",
				funcion: x.funcion
			};
			z.type = "DELETE";
			z.data = "";
			z.selfCallback = reporte.Salir;
			z.selfCallbackVars = params;
			z.json = true;
			z.callback = function (J) {
				if (!J["res"]) {// si hay error
					MJ_simple("Cerrando Sesión", J["mj"]);
				}else{//si ya cerró
					reporte.RevisarCookie();
				}
			};
			Ajax_(z);
		},
		/**
		 * Actualiza un reporte como leido
		 */
		POST: function (id) {
			//creamos otra variable para NO usar la global
			var z = {
				url: x.url,
				funcion: x.funcion
			};
			z.type = "POST";
			z.data = "id="+id;
			z.selfCallback = reporte.POST;
			z.selfCallbackVars = id;
			z.json = true;
			z.callback = function (J) {
				if (!J["res"]) {
					MJ_simple(titulo, J["mj"]);
					return false;
				}
			};
			Ajax_(z);
		},
		apagarAlerta: function (id) {
			reporte.POST(id);
			$("#alerta").hide(1000, function () {
				$(this).html("");
			});
		},
		coloresAlerta: function (color) {
			$("#alerta").toggleClass("red");
			setTimeout("reporte.coloresAlerta();", 500);
		},
		GET: function (params) {
			//creamos otra variable para NO usar la global
			var z = {
				url: x.url,
				funcion: x.funcion
			};
			z.type = "GET";
			z.data = "";
			z.selfCallback = reporte.GET;
			z.selfCallbackVars = params;
			z.json = true;
			z.callback = function (J) {
				if (!J["res"]) {
					MJ_simple(titulo, J["mj"]);
					return false;
				}
				var div = $("#listaMensajes");
				div.html("");
				totalEntra = J["bd"].length;
				if (total > -1 && total < totalEntra){
					iniciarAlerta(J["bd"][totalEntra-1]);
				}
				total=totalEntra;
				if (totalEntra > 0)
					J["bd"].map(function (e) {
						div.prepend("<div onclick='reporte.POST(" + e.id + ");' " + (e.atendido == true ? "" : "class='nuevo'") + "><span>" + e.fecha + "</span> " + Base64.decode(e.mensaje) + "</div>");
					});
				setTimeout("reporte.GET();", 10000);
			};
			Ajax_(z);
		}
	}
})();
reporte.init();

reporte.coloresAlerta(true);