# **EciBot: Agente inteligente para la interacción en Lenguaje Natural**

## **Desarrolladores**
- Kevin Julián Alvarado Pérez
- Kevin S. Sánchez Prieto

## **Directora de proyecto**
- Ing. Olga Patricia Álvarez Piñeiro

## **Probar**
*	[Probar Vesión Beta](http://chatbot.is.escuelaing.edu.co/demo)
---
## **Definición**
*	## Definición

	EciBot es el nuevo asistente virtual de la Escuela Colombiana de Ingeniería Julio Garavito, las personas pueden recibir asesoría y respuestas sobre los procesos de matrícula, cancelaciones de materias, verificar los horarios de asignaturas, reportar incidencias al área de audiovisuales, consultar números telefónicos de administrativos y profesores de planta, entre otras funcionalidades.

	Aún se sigue entrenando para atender las solicitudes de una forma más precisa.


*	## Antecedentes

	Hace 8 meses se concibió un proyecto de grado, cuyo objetivo se encaminó a conceptualizar e investigar los Chatbots y el papel que juega la **Inteligencia Artificial (AI)** para el desarrollo y optimización de esta herramienta informática. Inicialmente se pensó en diseñar un chatbot para diagnóstico de enfermedades, pero se decidió enfocarse en cómo beneficiar a la Escuela Colombiana de Ingeniería incorporando el uso de este tipo de herramienta. 

	La justificación de este enfoque se basa en el hecho de haber detectado que la incapacidad de atender personalmente las llamadas recibidas en el call center de la universidad, es percibida como desatención y esto ha redundado en pérdidas de aspirantes e interesados en servicios prestados por la Escuela Colombiana de Ingeniería Julio Garavito. En ese momento es que se decide que el proyecto identificará problemas similares y busca hacer uso de las tecnologías que combinan el uso de inteligencia artificial y servicios en la nube pueden utilizarse como medio de optimización de la atención al cliente.

---

## **Conociendo el desarrollo**
*	## Tecnologías usadas

	El desarrollo se llevó a cabo con las siguientes tecnologías
	*   **Microsoft Cognitive Services**
			
		Es un conjunto de APIs, SDKs y servicios disponibles para que los desarrolladores puedan crear aplicaciones inteligentes y atractivas. Tiene cinco categorías de servicios de inteligencia artificial: Visión, Voz, Lenguaje, Conocimiento y Búsqueda [leer más](https://azure.microsoft.com/es-es/services/cognitive-services/).

	*	**Microsoft Bot Framework**
		
		Permite construir, conectar, implementar y administrar el chatbot para que pueda ser accedido desde distintos medios cómo: sitio web, App, Cortana, equipos Microsoft, Skype, holgura, Facebook Messenger, y mucho más [leer más](https://dev.botframework.com/)

	*	**Microsoft Qna Maker** *(Questions and Answers)*
		
		Permite construir, entrenar y publicar un conjunto de preguntas y respuestas que va a atender el chatbot, como por ejemplo fechas de matrícula, dirección de la universidad, entre otros. [leer más](https://www.qnamaker.ai/)
		
	*	**Microsoft LUIS** *(Language Understanding Intelligent Service)*
		
		Permite el aprendizaje de máquinas para construir lenguaje natural en aplicaciones, chatbots y dispositivos de muchos. Crea rápidamente modelos personalizados para una correcta interpretación, basado en intentos y entidades [leer más](https://www.luis.ai/home).
		
	*	**NodeJS**
		
		Es un Runtime de JavaScript construido en el motor JavaScript V8 de Chrome. Node. js utiliza un modelo de e/s impulsado por eventos y sin bloqueo que lo hace ligero y eficiente. El ecosistema del paquete node. js es el mayor ecosistema de bibliotecas de código abierto en el mundo y es usado como el principal para desarrollar el chatbot [leer más](https://nodejs.org/en/).
		
	*	**JavaScript**
		
		Lenguaje de programación que se utiliza principalmente en su forma del lado del cliente, implementado como parte de un navegador web permitiendo mejoras en la interfaz de usuario y páginas web dinámicas, dónde para nuestro servicio es usado para el navegador de los sitios administrativos de los reportes generados por el chatbot[leer más](https://es.wikipedia.org/wiki/JavaScript).
		
	*	**Jquery**
		
		Es una biblioteca de JavaScript rápida, pequeña y rica en funciones. Hace que las cosas como los documentos HTML de desplazamiento y manipulación, manejo de eventos, animación y Ajax mucho más simple con una API fácil de usar que funciona a través de una multitud de navegadores. Con una combinación de versatilidad y extensibilidad, usada para que el navegador interprete eventos, código resumido y animaciones para las páginas administrativas de reportes que genera el chatbot [leer más](http://jquery.com/).
		
	*	**Ajax**
		
		Acrónimo de Asynchronous JavaScript And XML (JavaScript asíncrono y XML), es una técnica de desarrollo web que permite realizar peticiones y estas aplicaciones se ejecutan en el cliente, es decir, en el navegador de los usuarios mientras se mantiene la comunicación asíncrona con el servidor en segundo plano. De esta forma es posible realizar cambios sobre las páginas sin necesidad de recargarlas, mejorando la interactividad, velocidad y usabilidad en las aplicaciones, siendo usado para que la página web y el chatbot puedan hacer peticiones REST en segundo plano [leer más](https://es.wikipedia.org/wiki/AJAX).
		
	*	**HTML *v5***
		
		Sigla en inglés de HyperText Markup Language (lenguaje de marcas de hipertexto), hace referencia al lenguaje de marcado para la elaboración de páginas web. Es un estándar que sirve de referencia del software que conecta con la elaboración de páginas web en sus diferentes versiones, usado para la estructa de la página web administrador de los reportes que genere el chatbot [leer más](https://es.wikipedia.org/wiki/HTML)
		
	*	**CSS *v4***
		
		Acónimo en inglés Cascading Style Sheets, u Hojas de Estilo en Cascada, es la tecnología desarrollada por el World Wide Web Consortium (W3C) con el fin de separar la estructura de la presentación, siendo usado para la mejora visual del administrador de los reportes que genere el chatbot [leer más](http://www.maestrosdelweb.com/introcss/)
		
	*	**PHP *v7.0***
		
		Es un lenguaje popular de scripting de propósito general que es especialmente adecuado para el desarrollo web.
		Rápido, flexible y pragmático, php potencia todo desde blogs hasta los sitios web más populares del mundo, es usado para recibir y atender las peticiones REST [leer más](http://php.net).
		
	*	**Apache *v2***
		
		Es un servidor HTTP de código abierto para sistemas operativos modernos, incluyendo UNIX y Windows. Proporciona un servidor seguro, eficiente y extensible que con servicios http en sincronización con los estándares http actuales.
		El servidor HTTP Apache ("httpd") se lanzó en 1995 y ha sido el servidor Web más popular en Internet desde abril de 1996 [leer más](https://httpd.apache.org/).
		
	*	**Git**
		
		Es un software de control de versiones diseñado por Linus Torvalds, permitiendo la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones. permite llevar registro de los cambios en archivos de computadora y coordinar el trabajo que varias personas [leer más](https://es.wikipedia.org/wiki/Git).
		
	*	**API REST**
		
		Estandarización que permite realizar peticiones HTTP de forma que se puedan realizar consultas fácil y operaciones sobre un conjunto de datos [leer más](https://bbvaopen4u.com/es/actualidad/api-rest-que-es-y-cuales-son-sus-ventajas-en-el-desarrollo-de-proyectos)
		

*	## Modelo de funcionamiento
	A continuación se muestra como EciBot procesa los mensajes desde diferentes canales de conversación.

	![mapa](Manuales/Map/Flow_Map.PNG)

---

## Manuales para:
- [Manual de Instalación y requerimientos](Manuales/Instalacion.md)
- [Manual técnico](Manuales/Tecnico.md) (¿Cómo se encuentra configurado?)
- [Configuración proxy SSL](Manuales/ProxySSL.md) (En caso que se necesite colocar debajo de un **Proxy**)
- [Administrador](Manuales/Administrador.md)
